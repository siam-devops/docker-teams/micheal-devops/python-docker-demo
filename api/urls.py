from django.urls import path, include
from api.views import *
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static

router = routers.DefaultRouter()
# router.register('classified_detail_view',Classified_User_Details_View)


# WEB ROUTE URLS
web_base_url_v1 = "web/"
urlpatterns = [
    path('',include(router.urls)),
    
    # path('client_names_list/', 
    #     ClientNamesListAPI.as_view(),
    #     name="client_names_list"
    # ),
]