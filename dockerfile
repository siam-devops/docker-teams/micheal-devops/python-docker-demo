FROM python:3.6

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /home/docker-session/python-demo/python-docker-demo
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

EXPOSE 8002
CMD ["python", "manage.py", "runserver", "0.0.0.0:8002"]