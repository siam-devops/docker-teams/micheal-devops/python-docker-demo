Django==3.0.2
djangorestframework==3.11.0
django-filter==2.3.0
django-cors-headers==3.4.0
gunicorn==19.9.0
pillow==7.2.0
requests==2.24.0
razorpay==1.2.0